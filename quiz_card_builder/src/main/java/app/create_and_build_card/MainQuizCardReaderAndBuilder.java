package app.create_and_build_card;

import builder.QuizCardBuilder;
import reader.ReadFile;

import java.util.Scanner;

public class MainQuizCardReaderAndBuilder {
    public static void main(String[] args) {

        ReadFile readFile = new ReadFile();
        QuizCardBuilder builder = new QuizCardBuilder();

        System.out.println("0 - Exit the program;"
                + "\n1 - for create Quiz Card;"
                + "\n2 - for read \"Save.txt\" file;"
                + "\n\nEnter the number:");

        Scanner scanner = new Scanner(System.in);
        int nextDigit = scanner.nextInt();

        if (nextDigit == 1) {
            builder.go();
        } else if (nextDigit == 2) {
            readFile.readFile();
        } else if (nextDigit == 0) {
            System.exit(0);
        } else {
            System.out.println("Number is incorrect");
        }
    }
}
