package reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class ReadFile {
    public void readFile() {
        try {
            File myFile = new File("src/main/java/save_file/Save.txt");
            FileReader fileReader = new FileReader(myFile);

            BufferedReader reader = new BufferedReader(fileReader);

            /**
             * Создаем строковую переменную для временного хранения каждой строки в процессе чтения
             */
            String line = null;
            /**
             * Читаем строку и присваиваем ее строковой пременной "line"
             * Пока эта переменная не пуста, выводим на экран только что прочтенную строку
             */
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
