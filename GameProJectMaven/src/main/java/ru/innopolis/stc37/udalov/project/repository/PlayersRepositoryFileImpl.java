package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PlayersRepositoryFileImpl implements PlayersRepository {

    private final String playerDbFileName;
    private final String playerSequenceFileName;
    private Game game;


    public PlayersRepositoryFileImpl(String playerDbFileName, String playerSequenceFileName) {
        this.playerDbFileName = playerDbFileName;
        this.playerSequenceFileName = playerSequenceFileName;
    }

    @Override
    public Player findByPlayerName(String playerName) {
        Player player = null;

        try (BufferedReader reader = new BufferedReader(new FileReader(playerDbFileName))) {
            String line = reader.readLine();
            while (line != null) {
                if (line.contains(playerName)) {
                    String[] parsedLine = line.split("#");
                    player = new Player(Long.parseLong(parsedLine[0]),
                            parsedLine[1],
                            parsedLine[2],
                            Integer.parseInt(parsedLine[3]),
                            Integer.parseInt(parsedLine[4]),
                            Integer.parseInt(parsedLine[5]));
                    break;
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return player;
    }

    @Override
    public void save(Player player) {

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(playerDbFileName, true));
            player.setId(generateId());
            writer.write(player.getId() +
                    "#" + player.getPlayerIpAddress() +
                    "#" + player.getPlayerName() +
                    "#" + player.getMaxPoint() +
                    "#" + player.getWinCount() +
                    "#" + player.getLoseCount() + "\n");
            writer.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Player player) {

        try {
            List<String> playersList = new ArrayList<>(Files.readAllLines(Paths.get(playerDbFileName), StandardCharsets.UTF_8));

            for (int i = 0; i < playersList.size(); i++) {
                if (playersList.get(i).contains(player.getPlayerName())) {
                    playersList.set(i, player.getId() +
                            "#" + player.getPlayerIpAddress() +
                            "#" + player.getPlayerName() +
                            "#" + player.getMaxPoint() +
                            "#" + player.getWinCount() +
                            "#" + player.getLoseCount());
                    break;
                }
            }
            Files.write(Paths.get(playerDbFileName), playersList, StandardCharsets.UTF_8);

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    private Long generateId() {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(playerSequenceFileName));
            String lastIdGenerateString = reader.readLine();
            long id = Long.parseLong(lastIdGenerateString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(playerSequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
