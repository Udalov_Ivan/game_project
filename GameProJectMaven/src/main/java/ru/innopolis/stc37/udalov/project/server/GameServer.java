package ru.innopolis.stc37.udalov.project.server;

import ru.innopolis.stc37.udalov.project.dto.StatisticDto;
import ru.innopolis.stc37.udalov.project.services.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.innopolis.stc37.udalov.project.server.CommandsParser.*;

//сервер - подключение игроков по протоколу socket
public class GameServer {
    // отдельный поток (Thread) для первого игрока (параллельный main)
    private ClientThread firstPlayer;
    // отдельный поток (Thread) для второго игрока (параллельный main)
    private ClientThread secondPlayer;
    // объект для сокет-сервера
    private ServerSocket serverSocket;
    //флаг - определяет началась или нет игра
    private boolean isGameStarted = false;
    //игра в процессе
    private boolean isGameInProcess = true;
    //время начала игры
    private long startTimeMills;
    //идентификатор игры
    private long gameId;
    //обьект бизнес-логики игры
    private GameService gameService;
    //обьект синхронизации - мьютекс
    private Lock lock = new ReentrantLock();

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    // метод запуска сервера на определенном порту
    public void start(int port) {
        try {
            // запустили SocketServer на определенном порту
            serverSocket = new ServerSocket(port);
            System.out.println("SERVER STARTED...");
            // ждем первого игрока
            System.out.println("Waiting for the first player to connect...");
            // connect() будет висеть, пока не подключится игрок
            firstPlayer = connect();
            // ждем второго игрока
            System.out.println("Waiting for the second player to connect...");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    //ждет подключения игрока, возвращает обьект ClientThread, который представляет собой
    // отдельный поток в рамках которого происходит общение со стороны сервера с игроком
    private ClientThread connect() {
        Socket client;
        // получили игрока
        try {
            // уводит приложение в ожидание (wait) пока не присоединиться какой-либо игрок
            // как только игрок подключен к серверу
            // объект-соединение возвращается как результат выполнения метода
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // создали ему отдельный поток
        ClientThread clientThread = new ClientThread(client);
        //запустили поток
        clientThread.start();
        System.out.println("Player is connected...");
        // отправили игроку сообщение о том, что он подключен
        clientThread.sendMessage("You are connected to the server");
        return clientThread;
    }

    // отдельный поток для игрока
    private class ClientThread extends Thread {
        // что мы хотим отправить игроку
        private final PrintWriter toClient;
        // что мы хотим получить от игрока
        private final BufferedReader fromClient;

        //имя игроко из текущего потока
        private String playerNickname;
        //ip игрока из текущего потока
        private String ip;

        public ClientThread(Socket client) {
            try {
                //задача конструктора - получить потоки для чтения и записи
                // autoflush - чтобы сразу отправлял данные в поток, а не ждал пока
                // не вызовут принудительно flush
                //оборачиваем байтовые потоки в символьные потоки
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                //получение ip адреса
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        //run() - метод, который работает на протяжении всей программы
        @Override
        public void run() {
            //бесконечный цикл, работает пока игрок подключен и игра запущена
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    // получили сообщение от игрока
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                //если сообщение не пустое
                if (messageFromClient != null) {
                    //если сообщение начинается с префикса name:
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        //только один поток может завершить в конкретный момент времени игру
                        lock.lock();
                        isGameInProcess = false;
//                        sendMessage("final");
                        sendMessage(gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000).getMessage());
                        lock.unlock();

                    } else if (isMessageForMove(messageFromClient)) {
                        //передача сообщений о направлнии движения
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)) {
                        //передача сообщений о выстрелах
                        resolveShot(messageFromClient);
                    } else if (isMessageForDamage(messageFromClient)){
                        //передача сообщения о полученном уроне
                        resolveDamage();

                    }

                }
                //если оба игрока задали себе имя, и игра еще не началась
                //проверка - только один поток может выполнить проверку в один момент времени
                lock.lock();
                if (isGameReadyToStart()) {
                    //вызываем метод бизнес логики для начала игры
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(), firstPlayer.playerNickname, secondPlayer.playerNickname);
                    //устанавливаем флаг начала игры
                    startTimeMills = System.currentTimeMillis();
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private void resolveDamage() {
            if(meFirst()){
                //если игрок сделал выстрел
                gameService.shot(gameId, firstPlayer.playerNickname, secondPlayer.playerNickname);
                //если враг сделал выстрел
            } else {
                gameService.shot(gameId, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }

        private void resolveShot(String messageFromClient) {
            if (meFirst()){
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveMove(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveNickname(String messageFromClient) {
            //проверяем, если это первый игрок
            if (meFirst()) {
                //то даем первому игроку это имя
                firstPlayer.playerNickname = messageFromClient.substring(6);
                //то отправляем второму игроку, что первый игрок задал свое имя
                System.out.println("From first player: " + messageFromClient);
                secondPlayer.sendMessage(messageFromClient);
            } else {
                //иначе - это имя второго игрока
                secondPlayer.playerNickname = messageFromClient.substring(6);
                //если это второй игрок - отправляем первому информацию, о том, что второй
                // игрок дал себе имя
                System.out.println("From second player: " + messageFromClient);
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            //проверяем, что обьект потока - первый игрок
            return this == firstPlayer;
        }

        private boolean isGameReadyToStart() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }

        public String getIp() {
            return ip;
        }
    }
}
