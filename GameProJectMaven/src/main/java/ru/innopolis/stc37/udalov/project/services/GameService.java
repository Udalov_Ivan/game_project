package ru.innopolis.stc37.udalov.project.services;

import ru.innopolis.stc37.udalov.project.dto.StatisticDto;

public interface GameService {
    /**
     * Метод вызывается для обеспечения начала игры.
     * Если такой игрок уже есть, тоработаем с ним.
     * Если такого нет, то создаем нового.
     *
     * @param ipPlayerOne   IP-адрес первого игрока
     * @param ipPlayerTwo   IP-адрес второго игрока
     * @param playerOneName имя первого игрока
     * @param playerTwoName имя второго игрока
     * @return идентификатор игры
     */
    Long startGame(String ipPlayerOne, String ipPlayerTwo, String playerOneName, String playerTwoName);

    /**
     * фиксирует выстрелы игроков, попавшие в цель
     *
     * @param gameId          идентификатор игры
     * @param playerWhoShoot  имя первого игрока
     * @param playerWhoTarget имя второго игрока
     */
    void shot(Long gameId, String playerWhoShoot, String playerWhoTarget);

    StatisticDto finishGame(Long gameId, long second);

}
