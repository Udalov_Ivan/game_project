package ru.innopolis.stc37.udalov.project.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

//функциональный интерфейс, который преобразует одну строку в обьет
@FunctionalInterface
public interface RowMapper<T> {
    T mapRow(ResultSet row) throws SQLException;
}
