package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;

import java.util.ArrayList;
import java.util.List;

public class GamesRepositoryListImpl implements GamesRepository {

    private List<Game> games;

    public GamesRepositoryListImpl() {
        games = new ArrayList<>();
    }

    @Override
    public void save(Game game) {
        //идентификатор игры это ее порядковый номер в списке
        game.setId((long) games.size());
        games.add(game);
    }

    @Override
    public Game findById(Long gameId) {
        return games.get(gameId.intValue());
    }

    @Override
    public void update(Game game) {
        games.set(game.getId().intValue(), game);
    }

}
