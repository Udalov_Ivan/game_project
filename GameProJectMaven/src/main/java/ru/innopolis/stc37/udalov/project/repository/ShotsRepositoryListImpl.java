package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Shot;

import java.util.ArrayList;
import java.util.List;

public class ShotsRepositoryListImpl implements ShotsRepository {

    private List<Shot> shots;

    public ShotsRepositoryListImpl() {
        this.shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        this.shots.add(shot);

    }

    @Override
    public Shot findById(Long shotId) {
        return null;
    }

    @Override
    public void update(Shot shot) {

    }
}
