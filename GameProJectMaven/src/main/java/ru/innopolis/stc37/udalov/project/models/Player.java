package ru.innopolis.stc37.udalov.project.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Player {
    private Long id;
    private String playerIpAddress;
    private String playerName;
    private Integer maxPoint;
    private Integer winCount;
    private Integer loseCount;

    public Player(String playerIpAddress, String playerName, Integer maxPoint, Integer winCount, Integer loseCount) {
        this.playerIpAddress = playerIpAddress;
        this.playerName = playerName;
        this.maxPoint = maxPoint;
        this.winCount = winCount;
        this.loseCount = loseCount;
    }
}
