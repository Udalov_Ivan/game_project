package ru.innopolis.stc37.udalov.project.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.innopolis.stc37.udalov.project.dto.StatisticDto;
import ru.innopolis.stc37.udalov.project.repository.GamesRepository;
import ru.innopolis.stc37.udalov.project.repository.GamesRepositoryJdbcImpl;
import ru.innopolis.stc37.udalov.project.repository.PlayersRepository;
import ru.innopolis.stc37.udalov.project.repository.PlayersRepositoryJdbcImpl;
import ru.innopolis.stc37.udalov.project.repository.ShotsRepository;
import ru.innopolis.stc37.udalov.project.repository.ShotsRepositoryJdbcImpl;
import ru.innopolis.stc37.udalov.project.server.GameServer;
import ru.innopolis.stc37.udalov.project.services.GameService;
import ru.innopolis.stc37.udalov.project.services.GameServiceImpl;

import javax.sql.DataSource;

public class MainServer {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/game_project_db";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "post116";

    public static void main(String[] args) {
        //подключение с помощью Hikari ConnectionPool
        HikariConfig config = new HikariConfig();
        //данные для подключения
        config.setJdbcUrl(JDBC_URL);
        config.setDriverClassName("org.postgresql.Driver");
        config.setUsername(JDBC_USER);
        config.setPassword(JDBC_PASSWORD);
        //установить количество потоков подключений
        config.setMaximumPoolSize(20);
        //источник данных
        DataSource dataSource = new HikariDataSource(config);
        //создаем репозиторй для этого источника данных
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        //сервис который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        //передали сервис обьекту-серверу для игры
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
