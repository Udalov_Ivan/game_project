package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.models.Shot;
import ru.innopolis.stc37.udalov.project.utils.JdbcUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

import static ru.innopolis.stc37.udalov.project.utils.JdbcUtil.closeJdbcObjects;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT_SHOT = "INSERT INTO " +
            "shot (time_to_shoot, game, player_who_shoot, player_who_target) VALUES (?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_UPDATE_SHOT_BY_ID = "UPDATE shot SET " +
            "time_to_shoot = ?, game = ?, player_who_shoot = ?, player_who_target = ? WHERE id = ?";

    //language=SQL
    private static final String SQL_FIND_BY_SHOT_ID = "SELECT * FROM shot WHERE id = ?";

    //ссылка на метод, которая позволяет преобразовать строку из ResultSet в объект типа Shot
    private static final RowMapper<Shot> shotRowMapper = JdbcUtil::mapShotRow;

    private final DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             //RETURN_GENERATED_KEYS - запрос должен вернуть ключи(id), которые сгенерировала БД
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, shot.getTimeToShoot().toString());
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getPlayerWhoShoot().getId());
            statement.setLong(4, shot.getPlayerWhoIsTheTarget().getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            //если affectedRows не равен 1, то не получилось добавить выстрел
            if (affectedRows != 1) {
                throw new SQLException("Can't insert shoot!");
            }
            //создание id для нового выстрела
            //ключи которые база сгенерировала сама для этого запроса
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                //проверяем сгенерировала ли база что либо?
                if (generatedId.next()) {
                    //получаем ключ, который сгенерировала база для текущего запроса
                    shot.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
                } else {
                    throw new SQLException("Can't retrieve id!"); //если id не сгенерирован
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Shot findById(Long shotId) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_SHOT_ID)) {
            //кладем shotId в данный statement
            statement.setLong(1, shotId);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return shotRowMapper.mapRow(rows);
                }
            }
            return null;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_SHOT_BY_ID)) {
            statement.setString(1, shot.getTimeToShoot().toString());
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getPlayerWhoShoot().getId());
            statement.setLong(4, shot.getPlayerWhoIsTheTarget().getId());
            statement.setLong(5, shot.getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            //если affectedRows не равен 1, то не получилось добавить выстрел
            if (affectedRows != 1) {
                throw new SQLException("Can't update shot!");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
