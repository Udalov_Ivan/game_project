package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Shot;

public interface ShotsRepository {
    //сохранить выстрел
    void save(Shot shot);

    //найти игру по идентификатору
    Shot findById(Long shotId);

    //обновление информации о игре
    void update(Shot shot);
}

