package ru.innopolis.stc37.udalov.project.utils;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.models.Shot;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

public class JdbcUtil {

    private JdbcUtil() {
        throw new IllegalStateException("Utility class cannot be instantiated");
    }

    public static Game mapGameRow(ResultSet row) {
        try {
            return Game.builder()
                    .id(row.getLong("id"))
                    .dateOfGame(LocalDateTime.parse(row.getString("date_of_game")))
                    .playerOne(Player.builder()
                            .id(row.getLong("player_one"))
                            .build())
                    .playerTwo(Player.builder()
                            .id(row.getLong("player_two"))
                            .build())
                    .playerOneShootsCounters(row.getInt("player_one_shot_count"))
                    .playerTwoShootsCounters(row.getInt("player_two_shot_count"))
                    .timeOfGame(row.getLong("time_of_game"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Player mapPlayerRow(ResultSet row) {
        try {
            return Player.builder()
                    .id(row.getLong("id"))
                    .playerIpAddress(row.getString("player_ip_address"))
                    .playerName(row.getString("player_name"))
                    .maxPoint(row.getInt("max_point"))
                    .winCount(row.getInt("win_count"))
                    .loseCount(row.getInt("lose_count"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Shot mapShotRow(ResultSet row) {
        try {
            return Shot.builder()
                    .id(row.getLong("id"))
                    .timeToShoot(LocalDateTime.parse(row.getString("time_to_shoot")))
                    .game(Game.builder()
                            .id(row.getLong("game"))
                            .build())
                    .playerWhoShoot(Player.builder()
                            .id(row.getLong("player_who_shoot"))
                            .build())
                    .playerWhoIsTheTarget(Player.builder()
                            .id(row.getLong("player_who_target"))
                            .build())
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    //служебный метод закрывающий JDBC обьекты
    public static void closeJdbcObjects(Connection connection, Statement statement, ResultSet rows) {
        if (rows != null) {
            try {
                rows.close();
            } catch (SQLException ignore) {
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ignore) {
            }
        }
    }
}
