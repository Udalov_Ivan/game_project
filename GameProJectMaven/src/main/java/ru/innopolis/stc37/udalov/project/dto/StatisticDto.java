package ru.innopolis.stc37.udalov.project.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

//информация об игре (dto - Data Transfer Object/Обьекты передачи данных)
public class StatisticDto {

    private Long gameId;
    private String playerName;
    private Integer hit;
    private Integer maxPoint;
    private Long timeOfGame;
    private Integer winCount;
    private Integer loseCount;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public Integer getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(Integer maxPoint) {
        this.maxPoint = maxPoint;
    }

    public Long getTimeOfGame() {
        return timeOfGame;
    }

    public void setTimeOfGame(Long timeOfGame) {
        this.timeOfGame = timeOfGame;
    }

    public Integer getWinCount() {
        return winCount;
    }

    public void setWinCount(Integer winCount) {
        this.winCount = winCount;
    }

    public Integer getLoseCount() {
        return loseCount;
    }

    public void setLoseCount(Integer loseCount) {
        this.loseCount = loseCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticDto that = (StatisticDto) o;
        return Objects.equals(gameId, that.gameId) && Objects.equals(playerName, that.playerName) && Objects.equals(hit, that.hit) && Objects.equals(maxPoint, that.maxPoint) && Objects.equals(timeOfGame, that.timeOfGame) && Objects.equals(winCount, that.winCount) && Objects.equals(loseCount, that.loseCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId, playerName, hit, maxPoint, timeOfGame, winCount, loseCount);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" ");
        return sb.toString();
    }
}
