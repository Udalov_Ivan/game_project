Инструкция по запуску игры:
1. В GameProJectMaven запустить сервер MainServer.java (GameProJectMaven\src\main\java\ru\innopolis\stc37\udalov\project\app\MainServer.java);
2. В GameProjectClient запустить два раза клиент Main.java (GameProjectClient\src\main\java\ru\udalov\game\client\app\Main.java);
3. Нажать "Connection" в првом окне клиента, затем нажать "Connection" во втором окне клиента;
4. Ввести имя для первого клиента и нажать "Go", ввести имя для второго клиента и нажать "Go";

Игра ведется до 0 значения здоровья одного из игроков.

---

Управление:
Перемещение - клавиши WASD (а также на стрелки);
Выстрел - клавиша space bar;
Выход из клиента - клавиша Esk.
