public class PhraseRandomazer{

     public static void main(String []args){
        String[] wordListOne = {"метод критического пути", "на основе веб-технологий", "проникающий", "динамичный"};
        String[] wordListTwo = {"нестандартный ум", "чистый продукт", "использованный с выгодой", "ускоренный"};
        String[] wordListThree = {"процесс", "портал", "период времени", "обзор"};
        
        int oneLength = wordListOne.length;
        int twoLength = wordListTwo.length;
        int threeLength = wordListThree.length;
        
        int randOne = (int) (Math.random() * oneLength);
        int randTwo = (int) (Math.random() * twoLength);
        int randThree = (int) (Math.random() * threeLength);
        
        String phrase = wordListOne[randOne] + " " + wordListTwo[randTwo] + " " + wordListThree[randThree];
        
        System.out.println("Все, что нам нужно, - это " + phrase);
     }
}