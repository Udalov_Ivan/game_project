
public class Shop {
    //данные
    private int Cust[] = new int[50]; //массив ссылок на объект
    private int CustCount = 0; //счетчик числа объектов в контейнере
    private int CustSize; //текущий размер массива

    //набор методов
    //констуктор
    public Shop(int StartSize) {
        CustSize = StartSize;
        CustCount = 0;
        Cust = new int[CustSize]; //создание начально массива
    }

    //геттер для количества покупателей
    public int GetCustCount() {
        return CustCount;
    }

    ///////переписать
    //запрос ссылки по номеру покупателя
    //проверить number или вернуть Custs[number]
    public void GetCust(int number) {
        while (number != null) {
            if (number > 0 && number <= CustCount) {
                number = Cust[number];
            }
        }
    }

    /////переписать
    //изменение ссылки
    //проверить number ли вернуть false, либо установить Custs[number]=newCusts и вернуть true
    public boolean SetCust(int number, Customer newCustomer) {
        boolean res = false;
        if (!NoEmpty()) { //проверка заполненности массива
            if (newCustomer > 0) {
                Cust[number] = newCust;
                CustCount++;
                res = true;
            }
        }
        return res;
    }

    boolean NoEmpty() {
        boolean res = true; //полный

        if (CustCount < 50)
            res = false; // не полный, можно еще добавить

        return res;

        //добавление существующих покупателей
        public boolean AddCust ( int newCust){
            if (CustCount == CustSize) {
                CustSize = CustSize * 2;  //увеличить CustSize в 2 раза
                Customer[] tempCust = new Customer[CustSize];//создать новый массив
                //цикл for для переноса в новый массив
                for (int i = 0; i < CustSize; i++) {
                    tempCust[i] = Cust[i];
                }
                //выполнить переадресацию Custs=tempCusts
                Cust = tempCust;
            }
            //занести ссылку newCustomer в массив по индексу CustCount и увеличить счетчик
            Cust[CustCount] = new Cust;
            CustCount = CustCount + 1;
            return true;
        }

        //добавление с созданием
        public boolean AddCust (String aFam){
            //аналогично, пперед добавление ссылки создать объект класса Customer с помощью конструктора
            if (CustCount == CustSize) {
                CustSize = CustSize * 2;  //увеличить CustSize в 2 раза
                Customer[] tempCust = new Customer[CustSize];//создать новый массив
                //цикл for для переноса в новый массив
                for (int i = 0; i < CustSize; i++) {
                    tempCust[i] = Custs[i];
                }
                //выполнить переадресацию Custs=tempCusts
                Custs = tempCusts;
            }
            //констуктор ?
            Customer( int CustsSize){
                CustsSize = CustsSize;
                CustCount = 0;
                Custs = new Cust[CustSize]; //создание начально массива
            }

            //занести ссылку newCustomer в массив по индексу CustCount и увеличить счетчик
            Custs[CustCount] = aFam;
            CustCount = CustCount + 1;
            return true;
        }

        //удаление последнего покупателя в наборе
        public Customer DelCust (String CustSurname){
            int index = findIndex(CustSurname);
            if (index > 0) { //проверка наличия объектов в контейнере
                Custs[index] = null;// вернуть либо null
                CustCount--;  // либо ссылку на удаленный объект с уменьшением CustCoint
                return Custs[index];
            } else return null;
        }

        //поиск по фамилии покупателя
        public int FindCust (String aFam){
            for (int i = 0; i < CustCount; i++) //проход по элементам-ссылкам
            {
                if (Custs[i.GetFam == aFam]) {
                    return i;
                } //нашли- возвращаем номер покупателя в массиве
            } //конец оператора цикла
            return (-1); //не нашли- возвращаем фиктивный номер
        } //конец метода

        //подсчет средней стоимости покупок
        //цикл for для прохода по массиву с вызовом метода SredPrice
//итератор !
        public float CustSredPrice() {
            float CustSredPrice = 0;
            int counter = 0;
            int sum = 0;
            for (int i = 0; i < Cust.length; i++) {
                if (Cust[i] == null)
                    continue;
                if (Cust[i].count > 0) {
                    sum += Cust[i].sumCustomer() / Cust[i].count;
                    counter++;
                }
            }
            if (counter != 0)
                CustSredPrice = sum / counter;
            return CustSredPrice;
        }

//итератор!
        //получение всех данных
        public String GetAllCustData () {
            StringBuilder str = new StringBuilder();
//цикл for для прохода по массиву с вызовом метода GetData
            for (int i = 0; i < Cust.length; i++) {
                if (Cust[i] == null)
                    continue;
                str.append(Cust[i].GetData());
                str.append("\n \n");
            }
            return str.toString();
        }
    }
}
//конец описания класса