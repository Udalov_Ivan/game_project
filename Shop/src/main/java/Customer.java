public class Customer {

    //данные

    private String customerName;
    private int Buy[] = new int[5]; //массив на 5 покупок
    private int count = 0; //счетчик для массива

    //конструктор

    public Customer(String newCustomerName)
    {
        customerName=newCustomerName ;
    }

    //обработка фамилии покупателя
    //геттер для фамилии
    public String getCustomerName() {
        return customerName;
    }

    //сеттер для фамилии
    public void setNameCustomer(String newName) {
        customerName = newName;
    }

    //обработка покупок
    //геттер для покупки
    //возвращает стоимость покупки
    public int getBuy(int number) {
        int res = -1;
        if (number > 0 && number <= count) {
            res = Buy[number - 1];
        }
        return res;
    }


    //сеттер для покупки
    //добавляет новое значение в конец массива покупок
    public boolean setBuy (int newBuy) {
        boolean res = false;
        if (!NoEmpty()) { //проверка заполненности массива
            if (newBuy > 0) {
                Buy[count] = newBuy;
                count++;
                res = true;
            }
        }
        return res;
    }


    //изменение стоимости покупки по номеру
    //изменять нужно уже существующие покупки
    public boolean IzmBuy (int number, int newBuy) {
        boolean res = false;
        if (number <= count && number > 0)
            // стоимость не может быть отрицательной
            if (newBuy >= 0) {
                Buy[number - 1] = newBuy;
                res = true;
            }
        return res;
    }



    //проверка пустой ли массив
    boolean IsEmpty()
    {
        boolean res  = true; //пустой, можно еще добавить

        if (count > 0)
            res = false; // не пустой

        return res;
    }

    //проверка заполнености массива
    boolean NoEmpty()
    {
        boolean res  = true; //полный

        if (count < 5)
            res = false; // не полный, можно еще добавить

        return res;
    }


    //вычисление суммы покупок
    public int sumBuys ()
    {
        int s = 0;
        for(int i=0; i < count; i++)
        {
            s = s + Buy[i];
        }
        return s;
    }

    //вывод данных текстом
    public void outer()
    {
        System.out.println("Фамилия покупателя - " + getCustomerName() + ".");

        if (IsEmpty())
            System.out.println("У данного покупателя нет покупок.");
        else
            for (int i = 0; i < count; i++)
            {
                System.out.println("Покупка № " + (i+1) + " стоит " + Buy[i] + ".");
            }

        System.out.println("Итоговая стоимость покупок : " + sumBuys() + ".");
    }

}