public class ShopOne {
    //данные
    private int[] Custs = new int[50]; //массив ссылок на объект
    private int CustCount = 0; //счетчик числа объектов в контейнере
    private int CustSize; //текущий размер массива


    public ShopOne(int startSize) {
        CustSize = startSize;
        CustCount = 0;
        Custs = new int[CustCount];
    }

    //добавление существующих покупателей
    public boolean AddCust(int newCust) {
        Customer[] tempCust = new Customer[CustSize];//создать новый массив
        if (CustCount == CustSize) {
            CustSize = CustSize * 2;  //увеличить CustSize в 2 раза

            //цикл for для переноса в новый массив
            for (int i = 0; i < CustSize; i++) {
                tempCust[i] = Custs[i];
            }
            //выполнить переадресацию Custs=tempCusts
            Custs = tempCust;
        }
        //занести ссылку newCustomer в массив по индексу CustCount и увеличить счетчик
        Custs[CustCount] = Custs;
        CustCount = CustCount + 1;
        return true;
    }

    public int[] getCusts() {
        return Custs;
    }

    public void setCusts(int[] custs) {
        Custs = custs;
    }

    public int getCustCount() {
        return CustCount;
    }

    public void setCustCount(int custCount) {
        CustCount = custCount;
    }

    public int getCustSize() {
        return CustSize;
    }

    public void setCustSize(int custSize) {
        CustSize = custSize;
    }
}






