import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    public static void menu() {
        String newName;
        boolean res;
        int switcher = 0;

        Scanner sc = new Scanner(System.in);

        System.out.println("Введите фамилию нового клиента.");
        newName = sc.nextLine();
        Customer cust1 = new Customer (newName);

        do {
            System.out.println("\nЧто сделать? " +
                    "\nНапишите номер действия: " +
                    "\n 1 - Вывести все данные. " +
                    "\n 2 - Поменять фамилию клиента " +
                    "\n 3 - Добавить стоимость покупки. " +
                    "\n 4 - Поменять стоимость покупки по номеру. " +
                    "\n 5 - Вывести стоимость покупки по номеру. " +
                    "\n 6 - Завершить работу. \n");


            /**
             * оборачивает switcher в try/catch
             */
            try {
                switcher = sc.nextInt();
            } catch (NoSuchElementException e) {}


            switch (switcher) {

                //вывод всей информации
                case 1: {
                    cust1.outer();
                    break;
                }

                //изменение фамилии клиента
                case 2: {
                    System.out.println("Введите новую фамилию клиента.");
                    newName = sc.nextLine();
                    cust1.setNameCustomer(newName);
                    break;
                }

                //добавление покупки
                case 3: {
                    System.out.println("Введите стоимость новой покупки");
                    int newBuy = sc.nextInt();
                    res = cust1.setBuy(newBuy);
                    if (res) //добавлено
                        System.out.println("Покупка добавлена");
                    else
                        System.out.println("Ошибка!Покупка не добавлена");
                    break;
                }

                //изменение покупки по номеру
                case 4: {
                    System.out.println("Введите номер покупки, для которой нужно изменить стоимость");
                    int number = sc.nextInt();
                    System.out.println("Введите новую стоимость покупки");
                    int newBuy = sc.nextInt();
                    res = cust1.IzmBuy(number, newBuy);
                    if (res)
                        System.out.println("Покупка изменена.");
                    else
                        System.out.println("Ошибка!");
                    break;
                }

                //вывод покупки по номеру
                case 5: {
                    System.out.println("Введите номер от 1 до 10");
                    int numberOfBuy  = sc.nextInt();
                    int stPok = cust1.getBuy(numberOfBuy);

                    if (stPok == -1)
                        System.out.println("Неверно введен номер покупки!");
                    else
                        System.out.println("Покупка № " + numberOfBuy + " : стоимость - " + stPok);
                    break;
                }


                case 6: {
                    System.out.println("До новых покупок!\n");
                    break;
                }

                default: {
                    System.out.println("\nТакого действия не существует, введите число от 1 до 6!\n");
                }
            }

        } while (switcher != 6) ;
    }

    public static void main (String[]args){
        System.out.println("Здравствуйте.");
        menu();
    }

}