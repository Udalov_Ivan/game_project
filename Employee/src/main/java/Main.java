import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int act, mm, newSal;
        String newSurname;
        act = 0;

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите фамилию нового сотрудника");
        newSurname = sc.nextLine();
        Employee emp1 = new Employee(newSurname);

        do {
            System.out.println("\nКакие действия выполним? " +
                    "\nНапишите номер действия: " +
                    "\n 1 - Вывод " +
                    "\n 2 - Изменение фамилии сотрудника " +
                    "\n 3 - Добавление зарплаты " +
                    "\n 4 - Изменение зарплаты в определенном месяце " +
                    "\n 5 - Вывести зарплату в определенном месяце " +
                    "\n 6 - Выход \n");
            try {
                act = sc.nextInt();
            } catch (NoSuchElementException e) {
            }

            switch (act) {

                case 1: {
                    //вывод строки данных
                    emp1.getData();
                    break;
                }

                case 2: {
                    //изменение фамилии сотрудниика
                    System.out.println("Введите новую фамилию сотрудника");
                    newSurname = sc.nextLine();
                    emp1.setNameEmployee(newSurname);
                    break;
                }

                case 3: {
                    //добавление зарплаты в конец
                    System.out.println("Введите новую зарплату");
                    newSal = sc.nextInt();
                    emp1.setSalary(newSal);
                    break;
                }

                case 4: {
                    //изменение зарплаты в определенном месяце
                    System.out.println("Введите месяц в котором нужно изменить зарплату");
                    mm = sc.nextInt();
                    System.out.println("Введите новую зарплату");
                    newSal = sc.nextInt();
                    //изменение зарплаты в определенном месяце
                    emp1.addSalary(newSal, mm);
                    break;
                }

                case 5: {
                    System.out.println("Введите месяц");
                    mm = sc.nextInt();
                    System.out.println("В месяце " + mm + " - зарплата : " + emp1.getSalary(mm));
                    break;
                }

                case 6: {
                    System.out.println("Хорошего дня!\n");
                    break;
                }

                default: {
                    System.out.println("\nОшибка ввода, попробуйте снова!\n");
                }
            }

        } while (act != 6);
    }
}
