public class Employee {
    //данные
    private String employeeSurname;
    private int Salary[] = new int[12]; // массив на 12 зарплат
    private int count = 0;

    //конструктор
    public Employee(String newEmployeeSurname) {
        employeeSurname = newEmployeeSurname;
    }

    //геттер для фамилии
    public String getSurnameEmployee() {
        return employeeSurname;
    }

    //сеттер для фамилии
    public void setNameEmployee(String newEmployeeSurname) {
        employeeSurname = newEmployeeSurname;
    }

    //обработка зарплаты
    //геттер для зарплаты
    public int getSalary(int mm) {
        return Salary[mm - 1];
    }

    //сеттер для зарплаты (добавит в конец)
    public void setSalary(int newSal) {
        count++;
        for (int i = 0; i <= count; i++) {
            if (i == count - 1) {
                Salary[i] = newSal;
                break;
            }
        }
    }

    //изменение зарплаты по месяцу
    public void addSalary(int newSal, int mm) {
        for (int i = 0; i <= Salary.length; i++) {
            Salary[mm - 1] = newSal;
        }
    }

    //вычисление суммарных выплат
    public int sumSalary() {
        int s = 0;
        for (int i = 0; i <= count; i++) {
            s = s + Salary[i];
        }
        return s;
    }

    //вывод данных текстом
    public void getData() {
        System.out.println("Сотрудник " + 1 + ".");
        System.out.println(getSurnameEmployee());
        //вывод массива
        for (int i = 0; i <= Salary.length - 1; i++) {
            System.out.println("В месяце " + (i + 1) + " - зарплата : " + Salary[i]);
        }
        System.out.println("Количество выплат : " + count + ".");
        System.out.println("Суммарные выплаты составили : " + sumSalary() + ".");
    }
}
