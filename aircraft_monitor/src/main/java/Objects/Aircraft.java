package Objects;

public class Aircraft {

    String board;
    int serialNumber;
    int model;
    String dateOfManufactured;
    int time;
    int cycles;

    Unit unit;
    Task task;
    AirworthinessDirective airworthinessDirective;
    ServiceBulletin serviceBulletin;

    public Aircraft(String board, int serialNumber, int model, String dateOfManufactured) {
        if ((serialNumber > 0)&& (model > 0)) {
            this.board = board;
            this.serialNumber = serialNumber;
            this.model = model;
            this.dateOfManufactured = dateOfManufactured;
        } else {
            this.board = null;
            this.serialNumber = 0000;
            this.model = 0000;
            this.dateOfManufactured = null;
        }
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public String getDateOfManufactured() {
        return dateOfManufactured;
    }

    public void setDateOfManufactured(String dateOfManufactured) {
        this.dateOfManufactured = dateOfManufactured;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getCycles() {
        return cycles;
    }

    public void setCycles(int cycles) {
        this.cycles = cycles;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
        unit.aircraft = this;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
        task.aircraft = this;
    }

    public AirworthinessDirective getAirworthinessDirective() {
        return airworthinessDirective;
    }

    public void setAirworthinessDirective(AirworthinessDirective airworthinessDirective) {
        this.airworthinessDirective = airworthinessDirective;
        airworthinessDirective.aircraft = this;
    }

    public ServiceBulletin getServiceBulletin() {
        return serviceBulletin;
    }

    public void setServiceBulletin(ServiceBulletin serviceBulletin) {
        this.serviceBulletin = serviceBulletin;
        serviceBulletin.aircraft = this;
    }
}
