package paint;

import javax.sound.midi.ControllerEventListener;
import javax.sound.midi.ShortMessage;
import javax.swing.*;
import java.awt.*;

public class MusicDrawPanel extends JPanel implements ControllerEventListener {

    /**
     * Присваиваем флагу значение false, а когдаа будем получать событие, будем устанавливать true
     */
    boolean msg = false;

    @Override
    public void controlChange(ShortMessage event) {
        /**
         * Получили событие, присвоили флагу true и вызвали repaint()
         */
        msg = true;
        repaint();
    }

    public void paintComponent(Graphics g) {
        /**
         * Используем флаг msg, потому что другие обьекты могут запустить repaint()
         * Нам же нужно рисовать, когда произойдет событие ControllerEvent
         */
        if (msg) {
            /**
             * Генерируем случайный цвет и рисуем полупроизвольный прямоугольник
             */
            Graphics2D g2 = (Graphics2D) g;

            int r = (int) (Math.random() * 250);
            int gr = (int) (Math.random() * 250);
            int b = (int) (Math.random() * 250);

            g.setColor(new Color(r, gr, b));

            int ht = (int) ((Math.random() * 120) + 10);
            int width = (int) ((Math.random() * 120) + 10);
            int x = (int) ((Math.random() * 40) + 10);
            int y = (int) ((Math.random() * 40) + 10);
            g.fillRect(x, y, ht, width);
            msg = false;
        }
    }
}
