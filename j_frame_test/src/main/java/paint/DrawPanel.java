package paint;

import javax.swing.*;
import java.awt.*;

public class DrawPanel extends JPanel {
    public void paintComponent(Graphics g) {
        /**
         * Устанавливаем цвет для закрашивания всей панели
         */
        g.setColor(Color.black);
        /**
         * Начинаем закрашивание с 0 пикселлов от левого края и 0 от правого
         * Делаем ширину и высоту прямоугольника как у панели (this.getWidth(), this.getHeight())
         */
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        /**
         * Задаем случайные значения для цветов, которые будут меняться при нажатии кнопки
         * У каждого из RGB цветов 256 оттенков
         */
        int red = (int) (Math.random() * 255);
        int green = (int) (Math.random() * 255);
        int blue = (int) (Math.random() * 255);

        /**
         * Создаем обьукт цвета и задаем цвет тремя целыми числами
         */
        Color randomColor = new Color(red, green, blue);
        /**
         * Задаем цвет
         */
        g.setColor(randomColor);
        /**
         * Рисем фигуру на расстоянии 70 пикселей от левого и верхнего края
         * Ширину и высоту фигуры задаем в 100 пикселей
         */
        g.fillOval(90, 70, 100, 100);
    }
}
