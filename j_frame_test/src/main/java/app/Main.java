package app;

import panels.MiniMusicPlayer;
import panels.RectangleMusicPlayer;
import panels.SimpleAnimation;
import panels.SimpleBoxLayout;
import panels.SimpleCircleColorChange;
import panels.TextTestArea;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Press 1 - for circle change color; \n" +
                "Press 2 - for simple circle simpleAnimation; \n" +
                "Press 3 - for play note; \n" +
                "Press 4 - for random note with random rectangle; \n" +
                "Press 5 - for Flow Layout; \n" +
                "Press 6 - for Simple button text clicker; \n" +
                "\nEnter the number:");

        int inputNumber = scanner.nextInt();

        SimpleCircleColorChange simpleCircleColorChange = new SimpleCircleColorChange();
        SimpleAnimation simpleAnimation = new SimpleAnimation();
        MiniMusicPlayer miniMusicPlayer = new MiniMusicPlayer();
        RectangleMusicPlayer rectangleMusicPlayer = new RectangleMusicPlayer();
        SimpleBoxLayout boxLayout = new SimpleBoxLayout();
        TextTestArea testArea = new TextTestArea();

        switch (inputNumber) {
            case 1:
                /**
                 * Запускаем смену цвета круга
                 */
                simpleCircleColorChange.go();
                break;
            case 2:
                /**
                 * Запускаем простую анимацию перемещения круга
                 */
                simpleAnimation.go();
                break;
            case 3:
                /**
                 * Запускаем проигрывание нот и отображение в командной строке текста "Ля"
                 */
                miniMusicPlayer.go();
                break;
            case 4:
                /**
                 * Запускаем произвольные ноты и произвольные прямоугольники
                 */
                rectangleMusicPlayer.go();
                break;
            case 5:
                /**
                 * Вызываем вариант вертикальной компоновки кнопок
                 */
                boxLayout.go();
                break;
            case 6:
                /**
                 * При нажатии на кнопку в текстовом поли выводим текст о нажатии кнопки
                 */
                testArea.go();
                break;

        }

        System.out.println("\"If you want to exit - press 00\"");

        int exitNumber = scanner.nextInt();

        if (exitNumber == 00) {
            System.exit(0);
        }

    }
}
