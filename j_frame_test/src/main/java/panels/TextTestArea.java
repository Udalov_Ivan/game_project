package panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextTestArea implements ActionListener {

    private final int ROW = 10;
    private final int COLUMN = 20;

    JTextArea textArea;

    public void go() {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        JButton button = new JButton("Click!");
        button.addActionListener(this);
        /**
         * ROW - число строк (предпочитаемая высота)
         * COLUMN - число столбцов (определяет предпочитаемую ширину)
         */
        textArea = new JTextArea(ROW, COLUMN);
        /**
         * Включаем перенос текста
         */
        textArea.setLineWrap(true);

        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        panel.add(scrollPane);

        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.getContentPane().add(BorderLayout.SOUTH, button);

        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        textArea.append("Button was clicked! \n");
    }
}
