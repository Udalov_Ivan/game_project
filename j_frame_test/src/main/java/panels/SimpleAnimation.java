package panels;

import paint.DrawPanel;

import javax.swing.*;
import java.awt.*;

public class SimpleAnimation {
    int x = 70;
    int y = 70;

    public void go() {
        //System.out.println("Simple Circle animation");

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        InnerDrawPanel panel = new InnerDrawPanel();

        frame.getContentPane().add(panel);
        frame.setSize(300, 300);
        frame.setVisible(true);

        /**
         * Повторяем 130 раз
         */
        for (int i = 0; i < 130; i++) {
            /**
             * Увеличиваем координаты x и y
             */
            x++;
            y++;
            /**
             * Говорим панели перерисовать себя на новом месте
             */
            panel.repaint();
            /**
             * Замедляем процесс с помощью потоков, чтобы визуально наблюдать анимацию
             */
            try {
                Thread.sleep(50);
            } catch (Exception ex) {
            }
        }

    }

    /**
     * Создаем внутренний класс
     */
    class InnerDrawPanel extends JPanel {
        public void paintComponent(Graphics g) {
            /**
             * Закрашиваем прямоугольник начина я с координат x и y равных 0
             * и делаем его таким же как основная панель
             */
            g.setColor(Color.white);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(Color.green);
            /**
             * Используем постоянно обновляющиеся координаты x и y внешнего класса
             */
            g.fillOval(x, y, 40, 40);
        }
    }
}

