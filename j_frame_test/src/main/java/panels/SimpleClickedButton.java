package panels;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Реализуем интерфейс ActionListener и ActionEvent
 */
public class SimpleClickedButton implements ActionListener {
    JButton button;

    public static void main(String[] args) {
        SimpleClickedButton gui = new SimpleClickedButton();
        gui.go();
    }

    public void go() {
        JFrame frame = new JFrame();
        button = new JButton("Click me");
        /**
         * Регистрируем заинтересованность в кнопке
         */
        button.addActionListener(this);
        frame.setContentPane(button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    /**
     * Реализуем метод actionPerformed интерфейса ActionListener - метод обработки события
     * Кнопка вызывает этот метод, чтобы известить о наступлнии события
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        button.setText("I've been clicked!");
    }

}
