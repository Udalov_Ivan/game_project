package panels;

import javax.sound.midi.*;
import java.io.File;

/**
 * Для отслеживания события ControllerEvent реализуем интерфейс слушателя
 */
public class MiniMusicPlayer implements ControllerEventListener {
    public void go() {
        try {
            /**
             * Создаем и открываем синтезатор
             */
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            /**
             * Регистрируем события синтезатором
             * Метод отвечающий за регистрацию принимает обьект слушателя и целочисленный массив,
             * представляющий собой список событий ControllerEvent
             * В данном случае нужно событи #127
             */
            int[] eventsIWant = {127};
            sequencer.addControllerEventListener(this, eventsIWant);
            /**
             * Создаем последовательность и дорожку
             */
            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            for (int i = 5; i < 61; i += 4) {
                track.add(makeEvent(144, 1, i, 100, i));
                /**
                 * Ловим ритм - добовляем собственное событие ControllerEvent
                 * 176 - обозначает, что события - ControllerEvent
                 * аргумент для собыитя номер #127
                 * С помощью этого можно реагировать на воспроизведение каждой ноты
                 * Т.е. его цель - запуск чего-либо, что можно отслеживать
                 */
                track.add(makeEvent(176, 1, 127, 0, i));

                track.add(makeEvent(128, 1, i, 100, i + 2));
            }
            /**
             * Запускаем синтезатор
             */
            sequencer.setSequence(seq);
            sequencer.setTempoInBPM(220);
            sequencer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Вызываем метод makeEvent(), чтобы создать сообщение и событие, а затем добавляем
     * резкльтат (MidiEvent, полученное из makeEvent()) в дорожку
     * Они представляют собой пару включения и отключения (comd) воспроизведения ноты
     */
    public static MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        } catch (Exception e) {
        }
        return event;
    }

    /**
     * Метод обработки события
     * При каждом получении события мы пишем в командной строке слово "Ля"
     */
    @Override
    public void controlChange(ShortMessage event) {
        System.out.println("Ля");
    }
}
