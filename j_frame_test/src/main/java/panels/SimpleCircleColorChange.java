package panels;

import paint.DrawPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleCircleColorChange {

    JFrame frame;
    JButton button;
    JLabel label;

    public void go() {
        //System.out.println("Simple Circle color change");
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton labelButton = new JButton("Change Label");
        /**
         * Передаем методу регистрации слушателя кнопки экземпляр соответствующего класса слушателя
         */
        labelButton.addActionListener(new LabelListener());

        button = new JButton("Click to change colors");
        /**
         * Передаем методу регистрации слушателя кнопки экземпляр соответствующего класса слушателя
         */
        button.addActionListener(new ColorListener());

        label = new JLabel("Label");

        DrawPanel panel = new DrawPanel();

        frame.getContentPane().add(BorderLayout.SOUTH, button);
        frame.getContentPane().add(BorderLayout.CENTER, panel);

        frame.getContentPane().add(BorderLayout.EAST, labelButton);
        frame.getContentPane().add(BorderLayout.WEST, label);

        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    /**
     * Внутренний класс LabelListener для label
     */
    class LabelListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            label.setText("Changed");
        }

    }

    /**
     * Внутренний класс ColorListener для button
     */
    class ColorListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            button.setText("Color changed!");
            /**
             * При нажатии кнопки, вызываем для фрейма метод repaint()
             * Это значит, что метод paintComponent() (находится в классе DrawPanel)
             * вызывается для каждого виджета во фрейме
             */
            frame.repaint();
        }
    }

}
