package panels;

import javax.swing.*;
import java.awt.*;

public class SimpleBoxLayout {
    public void go() {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        // Задаем цвет панели - серый
        panel.setBackground(Color.darkGray);

        // Изменяем диспетчер компоновки на новый экземпляр BoxLayout
        // Y_AXIS для вертикального расположения
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Создаем кнопки
        JButton buttonOne = new JButton("Click me!");
        JButton buttonTwo = new JButton("And me!");

        // Добавляем кнопки на панель
        panel.add(buttonOne);
        panel.add(buttonTwo);

        // Добавляем панель внутрь фрейма
        frame.getContentPane().add(BorderLayout.EAST, panel);

        frame.setSize(250, 200);
        frame.setVisible(true);
    }
}
