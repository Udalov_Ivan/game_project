package panels;

import javax.swing.*;

public class SimpleButton {
    public static void main(String[] args) {

        /**
         * Создаем фрейм и кнопку
         * Конструктору кнопки передаем текст, который будет на ней отображаться
         */
        JFrame frame = new JFrame();
        JButton button = new JButton("Click me");
        /**
         * Добавляем завершение работы при закрытии окна
         */
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /**
         * Добавляем кнопку на панель фрейма
         */
        frame.getContentPane().add(button);
        /**
         * Присваиваем фрейму размер (в пикселях)
         */
        frame.setSize(300, 300);
        /**
         * Делаем фрейм видимым
         */
        frame.setVisible(true);
    }
}
