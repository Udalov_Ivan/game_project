public class QuickSort {
    //Быстрая сортировка массива
    public static void quickSort(int[] source, int left, int right) {

        int leftMarker = left;
        int rightMarker = right;

        int pivot = source[(leftMarker + rightMarker) / 2];

        do {
            // Двигаем левый маркер слева направо пока элемент меньше, чем pivot
            while (source[leftMarker] < pivot) {
                leftMarker++;
            }
            // Двигаем правый маркер, пока элемент больше, чем pivot
            while (source[rightMarker] > pivot) {
                rightMarker--;
            }
            // Нужно ли обменять местами элементы, на которые указывают маркеры
            if (leftMarker <= rightMarker) {
                // Если левый маркер будет меньше правого, но надо поменять их местами
                if (leftMarker < rightMarker) {
                    int temp = source[leftMarker];
                    source[leftMarker] = source[rightMarker];
                    source[rightMarker] = temp;
                }
                // Сдвигаем маркеры, чтобы получить новые границы
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        // Выполняем рекурсивно для частей
        if (leftMarker < right) {
            quickSort(source, leftMarker, right);
        }
        if (left < rightMarker) {
            quickSort(source, left, rightMarker);
        }
    }

    public static void quickSort(double[] source, int left, int right) {

        int leftMarker = left;
        int rightMarker = right;

        double pivot = source[(leftMarker + rightMarker) / 2];

        do {
            while (source[leftMarker] < pivot) {
                leftMarker++;
            }
            while (source[rightMarker] > pivot) {
                rightMarker--;
            }
            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    double temp = source[leftMarker];
                    source[leftMarker] = source[rightMarker];
                    source[rightMarker] = temp;
                }
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        if (leftMarker < right) {
            quickSort(source, leftMarker, right);
        }
        if (left < rightMarker) {
            quickSort(source, left, rightMarker);
        }
    }
}
