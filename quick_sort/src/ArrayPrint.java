public class ArrayPrint {
    //печать элементов массива
    public static void print(int[]  array, String text){

        System.out.print(text + " ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.print("\n");
    }

    public static void print(double[]  array, String text){

        System.out.print(text + " ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.print("\n");
    }


}
