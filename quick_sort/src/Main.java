public class Main {

    public static void main(String[] args) {

        int[] array = {10, 2, 10, 3, 1, -2, 5};

        ArrayPrint.print(array, "Исходный массив (Integer):");

        QuickSort.quickSort(array, 0, 6);

        ArrayPrint.print(array, "Отсортированный массив(Integer):");


        double[] arraySecond = {77.9, 14.2, 2.5, 7.5, -8.3, 9.11, 45.43, 1.0, 4.6};

        ArrayPrint.print(arraySecond,"Исходный массив (Double):");

        QuickSort.quickSort(arraySecond, 0, 8);

        ArrayPrint.print(arraySecond, "Отсортированный массив (Double):");

    }
}