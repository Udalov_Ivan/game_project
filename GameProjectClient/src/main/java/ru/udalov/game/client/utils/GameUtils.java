package ru.udalov.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.udalov.game.client.controllers.MainController;
import ru.udalov.game.client.socket.SocketClient;
import ru.udalov.game.client.sound.SoundClient;

//утилитный клас отвечающий за перемещение игрока
public class GameUtils {

    SoundClient soundClient = new SoundClient();

    //скорость перемещения игрока
    private static final int PLAYER_STEP = 5;
    //урон от попадания пули
    private static final int DAMAGE = 5;

    private AnchorPane pane;
    private MainController controller;
    private SocketClient client;

    //движение вправо
    public void goRight(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        //выполняем перемещение вправо, пока не пересекли правую границу
        if (!isIntersectsRightSeparator(player)) {
            movePlayerTankExplosionRight(player, tank, tankLeft, tankRight, tankDown, explosion);
        }
    }

    //движение влево
    public void goLeft(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        //выполняем перемещение влево, пока не пересекли левую границу
        if (!isIntersectsLeftSeparator(player)) {
            movePlayerTankExplosionLeft(player, tank, tankLeft, tankRight, tankDown, explosion);
        }
    }

    //движеник вверх
    public void goUp(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        //выполняем перемещение вверх, пока не пересекли верхнюю границу
        if (!isIntersectsUpperSeparator(player)) {
            movePlayerTankExplosionUp(player, tank, tankLeft, tankRight, tankDown, explosion);
        }
    }

    //движение вниз
    public void goDown(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        //выполняем перемещение вниз, пока не пересекли нижнюю границу
        if (!isIntersectsLowerSeparator(player)) {
            movePlayerTankExplosionDown(player, tank, tankLeft, tankRight, tankDown, explosion);
        }
    }

    //создание пули
    //player - стреляющий, isEnemy: false игрок стреляет во врага, true - враг стреляет в игрока
    public Circle createBulletFor(Circle player, boolean isEnemy) {
        //создаем пулю
        Circle bullet = new Circle();
        //задаем характеристики для пули
        bulletProperties(player, bullet);

        int value;

        if (isEnemy) {
            value = 1;
        } else {
            value = -1;
        }

        final Circle target;
        final Label targetHp;

        //определяем куда стреляем
        if (!isEnemy) {
            //во врага
            target = controller.getEnemy();
            targetHp = controller.getHpEnemy();
        } else {
            //в игрока
            target = controller.getPlayer();
            targetHp = controller.getHpPlayer();
        }

        //анимация пули
        //передаем задержку с которой делается каждый кадр анимации и саму анимацию
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bulletMoveAndDamage(isEnemy, bullet, value, target, targetHp);
        }));
        //количество выполнений анимации в цикле
        timeline.setCycleCount(500);
        //запуск анимации
        timeline.play();

        return bullet;
    }

    private void finishGame(Label hpLabel) {

        int hp = Integer.parseInt(hpLabel.getText());

        //изменение вида полоски здоровья в зависимости от количества очков здоровья
        changeHpBarr();

        //если здороье одного из игроков равно 0
        if (hp == 0) {
            //отправляем сообщение о завершении игры
            sendMessageExit();
            //воспроизводим звук завершения игры
            soundClient.playFinal();
            //выключаем визуализацию hp игрока
            if (controller.getHpPlayer().getText().equals("0")) {
                playerDefeat();
            }
            //выключаем визуализацию hp врага
            if (controller.getHpEnemy().getText().equals("0")) {
                enemyDefeat();
            }
        }

    }

    private boolean isIntersectsRightSeparator(Circle player) {
        return player.getBoundsInParent().intersects(controller.getSeparatorRight().getBoundsInParent());
    }

    private boolean isIntersectsUpperSeparator(Circle player) {
        return player.getBoundsInParent().intersects(controller.getUpperSeparator().getBoundsInParent());
    }

    private boolean isIntersectsLowerSeparator(Circle player) {
        return player.getBoundsInParent().intersects(controller.getLowerSeparator().getBoundsInParent());
    }

    private boolean isIntersectsLeftSeparator(Circle player) {
        return player.getBoundsInParent().intersects(controller.getSeparatorLeft().getBoundsInParent());
    }

    private boolean isIntersects(Circle bullet, Circle player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    private void playerDefeat() {
        controller.getHpPlayerImage().setVisible(false);
        controller.getGreenTank().setVisible(false);
        controller.getGreenTankDown().setVisible(false);
        controller.getGreenTankRight().setVisible(false);
        controller.getGreenTankLeft().setVisible(false);
        controller.getExpOne().setVisible(true);
    }

    private void enemyDefeat() {
        controller.getHpTargetImage().setVisible(false);
        controller.getYellowTank().setVisible(false);
        controller.getYellowTankDown().setVisible(false);
        controller.getYellowTankRight().setVisible(false);
        controller.getYellowTankLeft().setVisible(false);
        controller.getExpTwo().setVisible(true);
    }

    private void sendMessageExit() {
        //отправляем сообщение на сервер о завершении игры
        client.sendMessage("exit");
        //делае видимой информацию о результате игры и надпись Game is Finished
        controller.getGameFinished().setVisible(true);
        controller.getResultGame().setVisible(true);
        //делаем неактивным значение здоровья игроков
        controller.getHpPlayer().setDisable(true);
        controller.getHpEnemy().setDisable(true);
    }

    private void bulletMoveAndDamage(boolean isEnemy, Circle bullet, int value, Circle target, Label targetHp) {
        //движении пули наверх
        bullet.setCenterY(bullet.getCenterY() + value);

        //если пуля перечекает цель и она еще видна
        if (bullet.isVisible() && isIntersects(bullet, target)) {
            //отнимаем очки здоровья
            createDamage(targetHp);
            //скрываем пулю при пересечении с целью
            bullet.setVisible(false);
            //отправляем сообщение на сервер о нанесенном уроне выстрелом, если попали в цель
            if (!isEnemy) {
                client.sendMessage("DAMAGE");
                // Звук попадания в цель
                soundClient.playDamage();
            }
            //завершение игры
            finishGame(targetHp);
            bullet.setVisible(false);
        }
    }

    private void bulletProperties(Circle player, Circle bullet) {
        //радиус
        bullet.setRadius(5);
        //для того чтобы пуля появилась
        pane.getChildren().add(bullet);
        //чтобы пуля появлялась в центре игрока
        bullet.setCenterX(player.getCenterX() + player.getLayoutX());
        bullet.setCenterY(player.getCenterY() + player.getLayoutY());
        //цвет
        bullet.setFill(Color.ORANGE);
    }

    private void movePlayerTankExplosionRight(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        player.setCenterX(player.getCenterX() + PLAYER_STEP);

        tank.setX(tank.getX() + PLAYER_STEP);
        tankLeft.setX(tankLeft.getX() + PLAYER_STEP);
        tankRight.setX(tankRight.getX() + PLAYER_STEP);
        tankDown.setX(tankDown.getX() + PLAYER_STEP);

        explosion.setX(explosion.getX() + PLAYER_STEP);
    }

    private void movePlayerTankExplosionLeft(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        player.setCenterX(player.getCenterX() - PLAYER_STEP);

        tank.setX(tank.getX() - PLAYER_STEP);
        tankLeft.setX(tankLeft.getX() - PLAYER_STEP);
        tankRight.setX(tankRight.getX() - PLAYER_STEP);
        tankDown.setX(tankDown.getX() - PLAYER_STEP);

        explosion.setX(explosion.getX() - PLAYER_STEP);
    }

    private void movePlayerTankExplosionUp(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        player.setCenterY(player.getCenterY() - PLAYER_STEP);

        tank.setY(tank.getY() - PLAYER_STEP);
        tankLeft.setY(tankLeft.getY() - PLAYER_STEP);
        tankRight.setY(tankRight.getY() - PLAYER_STEP);
        tankDown.setY(tankDown.getY() - PLAYER_STEP);

        explosion.setY(explosion.getY() - PLAYER_STEP);
    }

    private void movePlayerTankExplosionDown(Circle player, ImageView tank, ImageView tankLeft, ImageView tankRight, ImageView tankDown, ImageView explosion) {
        player.setCenterY(player.getCenterY() + PLAYER_STEP);

        tank.setY(tank.getY() + PLAYER_STEP);
        tankLeft.setY(tankLeft.getY() + PLAYER_STEP);
        tankRight.setY(tankRight.getY() + PLAYER_STEP);
        tankDown.setY(tankDown.getY() + PLAYER_STEP);

        explosion.setY(explosion.getY() + PLAYER_STEP);
    }

    private void createDamage(Label hpLabel) {
        //изначальное значение здоровья преобразуем в int
        int hp = Integer.parseInt(hpLabel.getText());
        //пока здоровье не равно нулю
        if (hp != 0) {
            //отнимаем значение DAMAGE от здоровья и преобразуем обратно в текст
            hpLabel.setText(String.valueOf(hp - DAMAGE));
        }
    }

    private void changeHpBarr() {
        //отнимаем 25%
        if (controller.getHpPlayer().getText().equals("75")) {
            controller.getoFour().setVisible(false);
        }
        if (controller.getHpEnemy().getText().equals("75")) {
            controller.gettFour().setVisible(false);
        }
        //отнимаем 50%
        if (controller.getHpPlayer().getText().equals("50")) {
            controller.getoThree().setVisible(false);
        }
        if (controller.getHpEnemy().getText().equals("50")) {
            controller.gettThree().setVisible(false);
        }
        //отнимаем 75%
        if (controller.getHpPlayer().getText().equals("25")) {
            controller.getoTwo().setVisible(false);
        }
        if (controller.getHpEnemy().getText().equals("25")) {
            controller.gettTwo().setVisible(false);
        }
        //отнимаем 100%
        if (controller.getHpPlayer().getText().equals("0")) {
            controller.getoOne().setVisible(false);
        }
        if (controller.getHpEnemy().getText().equals("0")) {
            controller.gettOne().setVisible(false);
        }
    }

    public void setClient(SocketClient client) {
        this.client = client;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }

}
