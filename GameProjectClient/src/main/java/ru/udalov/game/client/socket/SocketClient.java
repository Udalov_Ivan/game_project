package ru.udalov.game.client.socket;

import javafx.application.Platform;
import ru.udalov.game.client.controllers.MainController;
import ru.udalov.game.client.utils.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

// поток для получения сообщений с сервера
public class SocketClient extends Thread {
    // канал подключения
    private Socket socket;
    // стрим для отправления сообщений серверу
    private PrintWriter toServer;
    // стрим для получения сообщений от сервера
    private BufferedReader fromServer;

    private MainController controller;

    private GameUtils gameUtils;


    public BufferedReader getFromServer() {
        return fromServer;
    }

    public SocketClient(MainController controller, String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    // ждем сообщений от сервера
    @Override
    public void run() {
        while (true) {
            String messageFromServer;
            try {
                // прочитали сообщение с сервера
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
//                    // печатаем в консоли сообщение с сервера
//                    System.out.println("From server: " + messageFromServer);

                    //вывод информации в поле Game Info
                    controller.getResultGame().setText(messageFromServer);

                    switch (messageFromServer) {
                        //если приходит сообщение left - двигаем противника влево
                        case "left":
                            ifYellowTankVisibleLeft();
                            gameUtils.goLeft(controller.getEnemy(), controller.getYellowTank(), controller.getYellowTankLeft(), controller.getYellowTankRight(), controller.getYellowTankDown(), controller.getExpTwo());
                            break;
                        //если приходит сообщени right - двигаем противника вправо
                        case "right":
                            ifYellowTankVisibleRight();
                            gameUtils.goRight(controller.getEnemy(), controller.getYellowTank(), controller.getYellowTankLeft(), controller.getYellowTankRight(), controller.getYellowTankDown(), controller.getExpTwo());
                            break;
                        //если приходит сообщени shot - отображаем выстрел противника
                        case "shot":
                            //запуск Java FX приложения в своем потоке
                            Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                            break;
                        //если приходит сообщение up - двигаем противника вверх
                        case "down":
                            ifYellowTankVisibleDown();
                            gameUtils.goUp(controller.getEnemy(), controller.getYellowTank(), controller.getYellowTankLeft(), controller.getYellowTankRight(), controller.getYellowTankDown(), controller.getExpTwo());
                            break;
                        //если приходит сообщение down - двигаем противника вниз
                        case "up":
                            ifYellowTankVisibleUp();
                            gameUtils.goDown(controller.getEnemy(), controller.getYellowTank(), controller.getYellowTankLeft(), controller.getYellowTankRight(), controller.getYellowTankDown(), controller.getExpTwo());
                            break;
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }

    //танк становиться видимым при движении вправо
    private void ifYellowTankVisibleRight() {
        controller.getYellowTank().setVisible(false);
        controller.getYellowTankRight().setVisible(true);
        controller.getYellowTankLeft().setVisible(false);
        controller.getYellowTankDown().setVisible(false);
    }

    //танк становиться видимым при движении влево
    private void ifYellowTankVisibleLeft() {
        controller.getYellowTank().setVisible(false);
        controller.getYellowTankRight().setVisible(false);
        controller.getYellowTankLeft().setVisible(true);
        controller.getYellowTankDown().setVisible(false);
    }

    //танк становиться видимым при движении вверх
    private void ifYellowTankVisibleUp() {
        controller.getYellowTank().setVisible(true);
        controller.getYellowTankRight().setVisible(false);
        controller.getYellowTankLeft().setVisible(false);
        controller.getYellowTankDown().setVisible(false);
    }

    //танк становиться видимым при движении вниз
    private void ifYellowTankVisibleDown() {
        controller.getYellowTank().setVisible(false);
        controller.getYellowTankRight().setVisible(false);
        controller.getYellowTankLeft().setVisible(false);
        controller.getYellowTankDown().setVisible(true);
    }
}
