package ru.udalov.game.client.sound;

import javax.sound.midi.*;

public class SoundClient {

    public void playBullet() {
        try {
            // Получаем синтезатор и открываем его, чтобы начать использовать
            Sequencer sint = MidiSystem.getSequencer();
            sint.open();

            // Создаем синтезатор
            Sequence seq = new Sequence(Sequence.PPQ, 4);

            // Запрашиваем трек у последовательности, трек содержится внутри последовательности,
            // а MIDI-данные - в треке
            Track track = seq.createTrack();

            // Создаем сообщение
            ShortMessage a = new ShortMessage();

            /**
             * Помещаем в сообщение инструкцию
             * Первый агрумент (command) - тип сообщения
             * channel - Канал
             * data1 - Нота для проигрывания
             * data2 - Скорость и сила нажатия клавиши
             */
            a.setMessage(144, 1, 30, 100);

            // Используя сообщение, создаем новое событие
            action(sint, seq, track, a);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void playDamage() {
        try {

            Sequencer sint = MidiSystem.getSequencer();
            sint.open();

            Sequence seq = new Sequence(Sequence.PPQ, 4);

            Track track = seq.createTrack();

            ShortMessage a = new ShortMessage();

            a.setMessage(144, 1, 50, 127);


            action(sint, seq, track, a);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void playFinal() {
        try {

            Sequencer sint = MidiSystem.getSequencer();
            sint.open();

            Sequence seq = new Sequence(Sequence.PPQ, 4);

            Track track = seq.createTrack();

            ShortMessage a = new ShortMessage();
            ShortMessage c = new ShortMessage();
            ShortMessage d = new ShortMessage();

            a.setMessage(144, 1, 50, 127);
            c.setMessage(144, 1, 20, 100);
            d.setMessage(144, 1, 90, 50);

            MidiEvent noteOn = new MidiEvent(a, 1);
            MidiEvent noteT = new MidiEvent(c, 2);
            MidiEvent noteD = new MidiEvent(d, 3);

            track.add(noteOn);
            track.add(noteT);
            track.add(noteD);

            action(sint, seq, track, a);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void playMove() {
        try {

            Sequencer sint = MidiSystem.getSequencer();
            sint.open();

            Sequence seq = new Sequence(Sequence.PPQ, 4);

            Track track = seq.createTrack();

            ShortMessage a = new ShortMessage();

            a.setMessage(144, 1, 10, 125);

            action(sint, seq, track, a);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void action(Sequencer sint, Sequence seq, Track track, ShortMessage a) throws InvalidMidiDataException {
        MidiEvent noteOn = new MidiEvent(a, 1);

        // Добавляем событие в трек
        track.add(noteOn);

        ShortMessage b = new ShortMessage();
        b.setMessage(128, 1, 44, 100);
        MidiEvent noteOff = new MidiEvent(b, 3);
        track.add(noteOff);

        // Передаем последовательность синтезатору
        sint.setSequence(seq);

        // Запускаем синтезатор
        sint.start();
    }
}
