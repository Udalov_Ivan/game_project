package sound.project.synth;

import sound.project.inteface.MusicalInstrument;

import javax.sound.midi.*;

public class MusicTest1 implements MusicalInstrument {

    @Override
    public void play() {

        try {
            /**
             * обьект Sequencer синтезирует композицию из информации в формате MIDI
             */

            Sequencer sequencer = MidiSystem.getSequencer();
            System.out.println("We get synthesizer");

            /**
             * создаем блок catch на тот случай,
             * если произойдет исключительная ситуация,
             * т.е. методом getSequencer() будет выдано исключениеMidiUnavailableException         *
             */
        } catch (MidiUnavailableException ex) {
            System.out.println("failure");
        }


    }
}
