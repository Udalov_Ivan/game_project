package sound.project.synth;

import sound.project.inteface.MusicalInstrument;

import javax.sound.midi.*;

public class Piano implements MusicalInstrument {

    @Override
    public void play() {
        try {
            /**
             * Получаем синтезатор и открываем его, чтобы начать использовать
             */
            Sequencer player = MidiSystem.getSequencer();
            player.open();
            /**
             * Создаем синтезатор
             */
            Sequence seq = new Sequence(Sequence.PPQ, 4);
            /**
             * Запрашиваем трек у последовательности, трек содержится внутри последовательности,
             * а MIDI-данные - в треке
             */
            Track track = seq.createTrack();
            /**
             * Создаем сообщение
             */
            ShortMessage a = new ShortMessage();
            ShortMessage c = new ShortMessage();
            ShortMessage d = new ShortMessage();
            /**
             * Помещаем в сообщение инструкцию
             * Первый агрумент (command) - тип сообщения
             * channel - Канал
             * data1 - Нота для проигрывания
             * data2 - Скорость и сила нажатия клавиши
             */
            a.setMessage(144, 1, 20, 100);
            /**
             * Используя сообщение, создаем новое событие
             */
            MidiEvent noteOn = new MidiEvent(a, 1);
            /**
             * Добавляем событие в трек
             */
            track.add(noteOn);

            ShortMessage b = new ShortMessage();
            b.setMessage(128, 1, 44, 100);
            MidiEvent noteOff = new MidiEvent(b, 3);
            track.add(noteOff);
            /**
             * Передаем последовательность синтезатору
             */
            player.setSequence(seq);
            /**
             * Запускаем синтезатор
             */
            player.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
