package sound.project.main;

import sound.project.synth.MusicCmdLine;

import java.util.Scanner;

public class MainMusicCmdLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MusicCmdLine cmdLine = new MusicCmdLine();

        System.out.println("Add args for instrument (0 - 127):");
        int instrument = scanner.nextInt();

        System.out.println("Add args for note (0 - 127):");
        int note = scanner.nextInt();

        if (note >= 0 && note <= 127 && instrument >= 0 && instrument <= 127) {
            cmdLine.play(instrument, note);
        }
        else if (note < 0 || note > 127) {
            System.out.println("Args for note is incorrect");
            System.exit(0);
        }
        else{
            System.out.println("Args for instrument is incorrect");
            System.exit(0);
        }
    }
}
