package sound.project.main;

import sound.project.synth.Piano;

import javax.sound.midi.*;

public class MainPiano {
    public static void main(String[] args) {

        Piano piano = new Piano();
        piano.play();

    }

}
