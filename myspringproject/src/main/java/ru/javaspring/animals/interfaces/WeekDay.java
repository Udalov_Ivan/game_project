package ru.javaspring.animals.interfaces;

public interface WeekDay {

    String getWeekDayName();
}
