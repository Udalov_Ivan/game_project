package ru.javaspring.animals.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.javaspring.animals.entities.Cat;
import ru.javaspring.animals.entities.Dog;
import ru.javaspring.animals.entities.Parrot;
import ru.javaspring.animals.interfaces.WeekDay;
import ru.javaspring.animals.week.*;

import java.time.DayOfWeek;
import java.time.LocalDate;

@Configuration
@ComponentScan("ru.javaspring.animals.entities")
public class MyConfig {

    // создаем бины
    @Bean
    public Cat getCat(Parrot parrot) {
        Cat cat = new Cat();
        //в бине можно использовать любой код для создания бина, например использовать имя из бина Parrot
        cat.setName(parrot.getName() + "-black");
        return cat;
    }

    // задаем явное имя для бина в скобках
    @Bean("dog")
    public Dog getDog() {
        return new Dog();
    }

    // задаем явное имя для бина в скобках, название метода может быть любым
    @Bean("parrot-blue")
    public Parrot weNeedMoreParrots() {
        return new Parrot();
    }

    @Bean
    public WeekDay getDay() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        switch (dayOfWeek) {
            case MONDAY:
                return new Monday();
            case TUESDAY:
                return new Tuesday();
            case WEDNESDAY:
                return new Wednesday();
            case THURSDAY:
                return new Thursday();
            case FRIDAY:
                return new Friday();
            case SATURDAY:
                return new Saturday();
            default:
                return new Sunday();
        }
    }

}
