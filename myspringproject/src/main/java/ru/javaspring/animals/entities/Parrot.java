package ru.javaspring.animals.entities;

import org.springframework.stereotype.Component;

//@Component("parrot-blue")
public class Parrot {
    private String name = "Blue";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
