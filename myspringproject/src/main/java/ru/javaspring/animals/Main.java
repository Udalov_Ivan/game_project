package ru.javaspring.animals;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.javaspring.animals.configs.MyConfig;
import ru.javaspring.animals.entities.Cat;
import ru.javaspring.animals.entities.Dog;
import ru.javaspring.animals.entities.Parrot;
import ru.javaspring.animals.interfaces.WeekDay;

public class Main {
    public static void main(String[] args) {
        // создаем пустой спринговый контекст, который будет искать свои бины по аннотациям в указанном пакете
        //ApplicationContext context = new AnnotationConfigApplicationContext("ru.javaspring.animals.entities");
        ApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);

        // просим Spring достать нам бины Cat
        Cat cat = context.getBean(Cat.class);
        // просим Spring достать нам бины Dog по имени бина "dog"
        Dog dog = (Dog) context.getBean("dog");
        // просим Spring достать нам бины Parrot по имени бина "parrot-blue" из конкретного класса Parrot
        Parrot parrot = context.getBean("parrot-blue", Parrot.class);

        // выводим имена животных на экран
        System.out.println(cat.getName());
        System.out.println(dog.getName());
        System.out.println(parrot.getName());

        // просим достать нам бины из интерфейса WeekDay
        WeekDay weekDay = context.getBean(WeekDay.class);
        // выводим на экран день недели соответствующий реальному
        System.out.println("It's " + weekDay.getWeekDayName() + " today!");
    }
}
