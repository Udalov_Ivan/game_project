package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Tuesday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "tuesday";
    }
}
