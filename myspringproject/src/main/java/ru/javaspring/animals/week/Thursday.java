package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Thursday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "thursday";
    }
}
