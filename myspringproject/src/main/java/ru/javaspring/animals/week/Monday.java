package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Monday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "monday";
    }
}
