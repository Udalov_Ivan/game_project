package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Saturday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "saturday";
    }
}
