package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Friday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "friday";
    }
}
