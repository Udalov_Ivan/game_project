package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Sunday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "sunday";
    }
}
