package ru.javaspring.animals.week;

import ru.javaspring.animals.interfaces.WeekDay;

public class Wednesday implements WeekDay {
    @Override
    public String getWeekDayName() {
        return "wednesday";
    }
}
