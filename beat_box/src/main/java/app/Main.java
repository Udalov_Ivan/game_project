package app;

import utils.BeatBox;

public class Main {
    public static void main(String[] args) {
        BeatBox beatBox = new BeatBox();
        beatBox.buildGUI();
    }
}
