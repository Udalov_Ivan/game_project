package utils;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class BeatBox {
    JPanel panel;
    /**
     * Храним флажки в массиве checkBoxArrayList
     */
    ArrayList<JCheckBox> checkBoxArrayList;
    Sequencer sequencer;
    Sequence sequence;
    Track track;
    JFrame frame;

    /**
     * Названия инструментов в виде строкового массива, для создания меток в пользовательском интерфейсе
     * на каждый ряд
     */
    String[] instrumentNames = {"Bass Drum", "Closed Hi-Hat", "Open Hi-Hat", "Acoustic Snare",
            "Crash Cymbal", "Hand Clap", "High Tom", "Hi Bongo",
            "Maracas", "Whistle", "Low Conga", "Cowbell",
            "Vibraslap", "Low-mid Tom", "High Agogo", "Open Hi Conga"};
    /**
     * Числа обозначающие фактические барабанные клавиши.
     */
    int[] instruments = {35, 42, 46, 38, 49, 39, 50, 60, 70, 72, 64, 56, 58, 47, 67, 63};

    public void buildGUI() {
        frame = new JFrame("Cyber BeatBox");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BorderLayout layout = new BorderLayout();
        JPanel background = new JPanel(layout);
        // пустая граница позволяет создать поля между краями панели и местом размещения компонентов
        background.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        checkBoxArrayList = new ArrayList<>();
        Box buttonBox = new Box(BoxLayout.Y_AXIS);

        JButton start = new JButton("Start");
        start.addActionListener(new MyStartListener());
        buttonBox.add(start);

        JButton stop = new JButton("Stop");
        stop.addActionListener(new MyStopListener());
        buttonBox.add(stop);

        JButton upTempo = new JButton("Tempo Up");
        upTempo.addActionListener(new MyUpTempoListener());
        buttonBox.add(upTempo);

        JButton downTempo = new JButton("Tempo Down");
        downTempo.addActionListener(new MyDownTempoListener());
        buttonBox.add(downTempo);

        JButton serialize = new JButton("Serialize");
        serialize.addActionListener(new SendListener());
        buttonBox.add(serialize);

        JButton restore = new JButton("Restore");
        restore.addActionListener(new ReadListener());
        buttonBox.add(restore);

        JButton origin = new JButton("Origin tempo");
        origin.addActionListener(new GroundZeroTempo());
        buttonBox.add(origin);

        Box nameBox = new Box(BoxLayout.Y_AXIS);
        for (int i = 0; i < 16; i++) {
            nameBox.add(new Label(instrumentNames[i]));
        }

        background.add(BorderLayout.EAST, buttonBox);
        background.add(BorderLayout.WEST, nameBox);

        frame.getContentPane().add(background);

        GridLayout gridLayout = new GridLayout(16, 16);
        gridLayout.setVgap(1);
        gridLayout.setHgap(2);
        panel = new JPanel(gridLayout);
        background.add(BorderLayout.CENTER, panel);

        /**
         * Создаем флажки и присваиваем им значение false (чтобы они не были установлены)
         * затем добавляем их в массив ArrayList и на панель
         */
        for (int j = 0; j < 256; j++) {
            JCheckBox checkBox = new JCheckBox();
            checkBox.setSelected(false);
            checkBoxArrayList.add(checkBox);
            panel.add(checkBox);
        }

        setUpMidi();

        frame.setBounds(50, 50, 300, 300);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * ПОлучаем синтезатор, секвенсор и дорожку
     */
    public void setUpMidi() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequence = new Sequence(Sequence.PPQ, 4);
            track = sequence.createTrack();
            sequencer.setTempoInBPM(120);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Создаем массив из 16 элементов, чтобы хранить значения для каждого инструмента
     * на все 16 тактов
     */
    public void buildTrackAndStart() {
        int[] trackList = null;

        /**
         * Избавляемся от старой дорожки и создаем новую
         */
        sequence.deleteTrack(track);
        track = sequence.createTrack();

        /**
         * Делаем это для каждого из 16 рядов
         */
        for (int h = 0; h < 16; h++) {
            trackList = new int[16];

            /**
             * Задаем клавишу, которая представляет инструмент
             * Массив содержит MIDI-числа для каждого инструмента
             */
            int key = instruments[h];

            /**
             * Делаем это для каждого такта текущего ряда
             */
            for (int g = 0; g < 16; g++) {
                JCheckBox checkBox = (JCheckBox) checkBoxArrayList.get(g + (16 * h));

                /**
                 * Если флажок установлен в такте, то помещаем значение клавиши в текущую ячейку
                 * массива (ячейку, которая представляем такт)
                 * Если нет, то инструмент не должен играть в этом такте,
                 * поэтому присваиваем ему 0
                 */
                if (checkBox.isSelected()) {
                    trackList[g] = key;
                } else {
                    trackList[g] = 0;
                }
            }

            /**
             * Для всех 16 тактов создается событие и добавляется в дорожку
             */
            makeTracks(trackList);
            track.add(makeEvent(176, 1, 127, 0, 16));
        }

        /**
         * Всегда нужно быть уверенным, что событие на такте 16 существует
         * (они идут от 0 до 15)
         * Иначе BeatBox может не пройти все 16 тактов перед тем, как
         * заново начнет последовательность
         */
        track.add(makeEvent(192, 9, 1, 0, 15));
        try {
            sequencer.setSequence(sequence);
            // Задаем количество повторений цикла (в данном случае - непрерывный цикл)
            sequencer.setLoopCount(sequencer.LOOP_CONTINUOUSLY);
            sequencer.start();
            sequencer.setTempoInBPM(120);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Внутренние классы для кнопок
     */
    public class MyStartListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            buildTrackAndStart();
            System.out.println("Play");
        }
    }

    public class MyStopListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            sequencer.stop();
            System.out.println("Stop");
        }
    }

    /**
     * Коэфициент темпа определяет темп синтезатора
     * По умолчанию от равен 1, по щелчку кнопки изменяем его на +/- 3%
     */
    public class MyUpTempoListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            float tempoFactor = sequencer.getTempoFactor();
            double tempoUp = tempoFactor * 1.03;
            sequencer.setTempoFactor((float) (tempoUp));
            System.out.println("Tempo up is - " + tempoUp);
        }
    }

    public class MyDownTempoListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            float tempoFactor = sequencer.getTempoFactor();
            double tempoDown = tempoFactor * 0.97;
            sequencer.setTempoFactor((float) (tempoDown));
            System.out.println("Tempo down is - " + tempoDown);
        }
    }

    /**
     * Внутренний класс приводящй темп к исходному значению
     */
    public class GroundZeroTempo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            double originTempo = 1.0f;
            sequencer.setTempoFactor((float) originTempo);
            System.out.println("Tempo return to origin - " + originTempo);
        }
    }

    /**
     * Создаем события для одного инструмента за каждый проход цикла для всех 16 тактов
     * Можно получить int[] для Bass Drum и каждый элемент массива будет содетжать
     * либо клавишу этого инструмента, либо ноль
     * Если это ноль - инструмент не играет на текущем такте
     * Иначе создаем событие и добавляем его в дорожку
     */
    public void makeTracks(int[] list) {
        for (int i = 0; i < 16; i++) {
            int key = list[i];

            if (key != 0) {
                /**
                 * Создаем событие включения/выключения и добавляем в дорожку
                 */
                track.add(makeEvent(144, 9, key, 100, i));
                track.add(makeEvent(128, 9, key, 100, i + 1));
            }
        }
    }

    public MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage shortMessage = new ShortMessage();
            shortMessage.setMessage(comd, chan, one, two);
            event = new MidiEvent(shortMessage, tick);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return event;
    }

    /**
     * Внутренний класс для сохранения последовательности флажков
     */
    public class SendListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            /**
             * Создаем булев массив для хранения состояния каждого флажка
             */
            boolean[] checkBoxState = new boolean[256];

            /**
             * Пробегаем чероез checkBoxArrayList, считываем состояния и добавляем полученные значения в булев массив
             */
            for (int i = 0; i < 256; i++) {
                JCheckBox checkBox = (JCheckBox) checkBoxArrayList.get(i);
                if (checkBox.isSelected()) {
                    checkBoxState[i] = true;
                }
            }

            /**
             * Сериализуем булев массив
             */
            try {
                FileOutputStream fileStream = new FileOutputStream(new File("CheckBox.ser"));
                ObjectOutputStream outputStream = new ObjectOutputStream(fileStream);
                outputStream.writeObject(checkBoxState);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            System.out.println("save performed");
        }
    }

    /**
     * Внутренний класс для восстановления последовательности флажков
     */
    public class ReadListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean[] checkBoxState = null;
            /**
             * Считываем обьект из файла и определяем его как булев массив
             */
            try {
                FileInputStream fileInputStream = new FileInputStream(new File("CheckBox.ser"));
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                checkBoxState = (boolean[]) objectInputStream.readObject();
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            /**
             * Восстанавливаем состояние каждого флажка в ArrayList,
             * содержащий обьекты JCheckBox (checkBoxArrayList)
             */
            for (int i = 0; i < 256; i++) {
                JCheckBox checkBox = (JCheckBox) checkBoxArrayList.get(i);
                if (checkBoxState[i]) {
                    checkBox.setSelected(true);
                } else {
                    checkBox.setSelected(false);
                }
            }

            System.out.println("loaded");

            /**
             * Останавливаем проигрывание мелодии и восстанавливаем последовательноть,
             * используя новые состояния флажков в ArrayList
             */
            sequencer.stop();
            buildTrackAndStart();
        }
    }
}
