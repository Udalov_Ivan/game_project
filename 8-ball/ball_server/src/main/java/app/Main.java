package app;

import server.EightBallServer;

public class Main {
    public static void main(String[] args) {
        EightBallServer eightBallServer = new EightBallServer();
        eightBallServer.serverStart();
    }
}
