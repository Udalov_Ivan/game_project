package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EightBallServer {
    /**
     * Создаем список предсказаний
     */
    String[] predictionList = {"Бесспорно",
            "Предрешено",
            "Никаких сомнений",
            "Определенно да",
            "Можешь быть уверен в этом",
            "Мне кажется - да",
            "Вероятнее всего",
            "Хорошие перспективы",
            "Знаки говорят - да",
            "Да",
            "Пока не ясно, попробуй снова",
            "Спроси позже",
            "Лучше не рассказывать",
            "Сейчас нельзя предсказать",
            "Сконцентрируйся и спроси опять",
            "Даже не думай",
            "Мой ответ - нет",
            "По моим данным - нет",
            "Перспективы не очень хорошие",
            "Весьма сомнительно"};

    public void serverStart() {
        try {

            /**
             * С помощью ServerSocket приложение отслеживает клиентские запросы на порту 4242 локально,
             * где выполняетсяданный код
             */
            ServerSocket serverSocket = new ServerSocket(4242);

            /**
             * Сервер находится в постоянном цикле ожидания клиентских подключений (и обслуживая их)
             */
            while (true) {
                /**
                 * Метод accept блокирует приложение до тех пор, пока не поступит запрос, после чего возвращает сокет
                 * для взаимодействия с клиентом
                 */
                Socket socket = serverSocket.accept();

                /**
                 * Используем соединение обьекта Socket с клиентом для создания экземпляра PrintWriter,
                 * после чего отправляем его с помощью (println()) строку с предсказанием, затем закрываем сокет
                 */
                PrintWriter writer = new PrintWriter(socket.getOutputStream());
                String prediction = getPredictionList();
                writer.println(prediction);
                writer.close();
                System.out.println(prediction);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Метод получения случайного предсказания
     */
    private String getPredictionList() {
        int randomPrediction = (int) (Math.random() * predictionList.length);
        return predictionList[randomPrediction];
    }
}
