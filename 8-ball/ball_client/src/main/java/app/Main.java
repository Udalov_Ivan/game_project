package app;

import client.EightBallClient;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        EightBallClient client = new EightBallClient();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Чтобы получить предсказание, напиши \"Предсказание\":");

        String takePredictionString = scanner.nextLine();

        if (takePredictionString.equals("Предсказание") || takePredictionString.equals("предсказание")){
            client.clientStart();
        } else {
            System.exit(0);
        }
    }
}
