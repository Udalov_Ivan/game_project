package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class EightBallClient {
    public void clientStart() {
        try {
            /**
             * Создаем соединение через сокет к приложению, работающему на порту 4242
             * на локальном компьютере
             */
            Socket socket = new Socket("127.0.0.1", 4242);

            InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());

            /**
             * Подключаем BufferedReader к InputStreamReader (который уже соединен с исходящим потоком сокета)
             */
            BufferedReader reader = new BufferedReader(streamReader);

            /**
             * Метод readLine работает так же, если бы BufferedReader был подключен к файлу
             */
            String prediction = reader.readLine();
            System.out.println("\n" + "Magic ball 8 говорит: " + prediction);


            /**
             * Закрываем все потоки
             */
            reader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
