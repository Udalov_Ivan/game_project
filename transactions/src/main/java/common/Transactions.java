package common;

public class Transactions {
    // Изначально на счету 100
    private int balance = 100;

    public int getBalance() {
        return balance;
    }

    public void withdraw(int amount) {
        balance = balance - amount;
    }

}


