package common;

public class TransactionsWork implements Runnable {

    //Один экземпляр Transactions, оба потока получают доступ только к одному банковскому счету
    private Transactions account = new Transactions();

    // Поток зацикливается и при каждой итерации пытается снять деньги со счета,
    // после снятия он еще раз проверяет баланс, чтобы убедиться в непревышении лимита
    public void run() {
        for (int x = 0; x < 10; x++) {
            makeWithdrawl(10);
            if (account.getBalance() < 0) {
                System.out.println("Limit is over");
            }
        }
    }

    // Проверяем баланс, если на счету не достаточно денег - выводим сообщение
    // Если, средств хватает - приостанавливаем поток, затем возобновляем, чтобы завершить транзакцию
    private void makeWithdrawl(int amount) {
        if (account.getBalance() >= amount) {
            System.out.println(Thread.currentThread().getName()
                    + " try to take many;"
                    + "\nmoney - "
                    + account.getBalance()
                    + ";");
            try {
                System.out.println(Thread.currentThread().getName()
                        + " Go to sleep"
                        + "\nmoney - "
                        + account.getBalance()
                        + ";");
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()
                    + " wake up"
                    + "\nmoney - "
                    + account.getBalance()
                    + ";");
            account.withdraw(amount);
            System.out.println(Thread.currentThread().getName()
                    + " finish transaction"
                    + "\nmoney - "
                    + account.getBalance()
                    + ";");
        } else {
            System.out.println("Sorry, but "
                    + Thread.currentThread().getName()
                    + " does not have enough money"
                    + "\nmoney - "
                    + account.getBalance()
                    + ";");
        }
    }
}
