package app;

import common.TransactionsWork;

public class Main {
    public static void main (String [] args){
        // Экземпляр Runnuble
        TransactionsWork theJob = new TransactionsWork();
        // Создаем два потока, которые работают с одним экземпляром счета, находящимся в классе Runnable
        Thread one = new Thread(theJob);
        Thread two = new Thread(theJob);
        one.setName("First");
        two.setName("Second");
        one.start();
        two.start();
    }
}
