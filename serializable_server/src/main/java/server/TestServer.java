package server;

import character.GameCharacter;

import java.io.*;
import java.util.Arrays;

public class TestServer {

    GameCharacter one = new GameCharacter(50, "Elf", new String[]{"Bow", "Sword", "Hand"});
    GameCharacter two = new GameCharacter(200, "Troll", new String[]{"Hand", "Axe"});
    GameCharacter three = new GameCharacter(120, "Sorcerer", new String[]{"Magic stick", "invisibility cloak"});

    public void saveCharInfo() {

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("GameSave.ser"));
            objectOutputStream.writeObject(one);
            objectOutputStream.writeObject(two);
            objectOutputStream.writeObject(three);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * Присваиваем значение null, чтоюбы нельзя было обратиться к обьекиам в куче
         */
        one = null;
        two = null;
        three = null;
    }

    public void readSaveFile() {
        /**
         * Читаем информацию из файла
         */
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("GameSave.ser"));
            GameCharacter oneRestore = (GameCharacter) objectInputStream.readObject();
            GameCharacter twoRestore = (GameCharacter) objectInputStream.readObject();
            GameCharacter threeRestore = (GameCharacter) objectInputStream.readObject();
            /**
             * Выводим информацию в консоль
             * Arrays.toString позволяет вывести текст из массива
             */
            printInfo(oneRestore, twoRestore, threeRestore);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printInfo(GameCharacter oneRestore, GameCharacter twoRestore, GameCharacter threeRestore) {
        System.out.println("Name - " + oneRestore.getName() +
                "; Power - " + oneRestore.getPower() +
                "; Weapons: " + Arrays.toString(oneRestore.getWeapons()) + ";");
        System.out.println("Name - " + twoRestore.getName() +
                "; Power - " + twoRestore.getPower() +
                "; Weapons: " + Arrays.toString(twoRestore.getWeapons()) + ";");
        System.out.println("Name - " + threeRestore.getName() +
                "; Power - " + threeRestore.getPower() +
                "; Weapons: " + Arrays.toString(threeRestore.getWeapons()) + ";");
    }
}
