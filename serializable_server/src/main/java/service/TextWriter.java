package service;

import java.io.FileWriter;
import java.io.IOException;

public class TextWriter {

    /**
     * Метод записи текстовой информации в файл
     */
    public void writeText(String text) {
        try {
            FileWriter writer = new FileWriter("File_For_Text.txt");
            writer.write(text + "\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
