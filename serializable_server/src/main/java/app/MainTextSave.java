package app;

import service.TextWriter;

import java.util.Scanner;

public class MainTextSave {
    public static void main(String[] args) {
        System.out.println("Input text you need to save:");
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();

        TextWriter textWriter = new TextWriter();
        textWriter.writeText(a);
    }
}
