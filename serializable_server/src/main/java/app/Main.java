package app;

import server.TestServer;

public class Main {
    public static void main(String[] args) {
        TestServer server = new TestServer();
        /**
         * Сохроняем информацию в файле
         */
        server.saveCharInfo();
        /**
         * Выводим информацию в консоль
         */
        server.readSaveFile();
    }

}
