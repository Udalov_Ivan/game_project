package character;

import java.io.Serializable;

/**
 * Реализуем разметочный (теговый) интерфейс Serializable
 * Обьекты класса GameCharacter сохранятся с применением сериализации
 */
public class GameCharacter implements Serializable {

    int power;
    String name = null;
    String[] weapons;
    /**
     * При сериализации пропускаем переменнуб помеченную "transient"
     * переменная будет восстановлена как пустая для ссылок набьекты или значение по умалчанию,
     * для простых типов
     */
    transient String option = "Option";

    public GameCharacter(int power, String name, String[] weapons) {
        this.power = power;
        this.name = name;
        this.weapons = weapons;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeapons(String[] weapons) {
        this.weapons = weapons;
    }

    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }

    public String[] getWeapons() {
        return weapons;
    }
}
