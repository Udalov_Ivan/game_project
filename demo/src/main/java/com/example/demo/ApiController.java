package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Возвращаем все записи из базы в JSON формате,
 * чтобы потом их можно было читать на клиенте.
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    final VisitRepository visitRepository;

    public ApiController(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @GetMapping("/visits")
    public Iterable<Visit> getVisit() {
        return visitRepository.findAll();
    }
}
