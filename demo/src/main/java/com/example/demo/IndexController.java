package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс, помеченный как @Controller автоматически регистрируется в MVC роутере,
 * а используя аннотации @(Get|Post|Put|Patch)Mapping можно регистрировать разные пути
 */
@Controller
public class IndexController {

    /**
     * Чтобы использовать репозиторий в контроллере воспользуемся механизмом внедрения зависимостей,
     * предоставляемый Spring Framework.
     * Чтобы это сделать нужно объявить зависимость контроллере.
     */
    final VisitRepository visitRepository;

    /**
     * Увидев в конструкторе параметр типа VisitRepository,
     * Spring найдет созданный Spring Data-ой репозиторий и передаст его в конструктор.
     */
    public IndexController(VisitRepository visitRepository){
        this.visitRepository = visitRepository;
    }
    /**
     * Метод возвращает ModelAndView — дальше Spring знает, что нужно взять вью index.mustache
     * из папки resources/templates (это соглашение по умолчанию) и передать туда модель
     */
    @GetMapping("/")
    public ModelAndView index() {
        Map<String, String> model = new HashMap<>();
        model.put("name", "Simple_Name");

        Visit visit = new Visit();
        visit.description = String.format("Visited at %s", LocalDateTime.now());
        visitRepository.save(visit);

        return new ModelAndView("index", model);
    }
}
