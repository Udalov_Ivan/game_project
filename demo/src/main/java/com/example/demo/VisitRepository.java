package com.example.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Благодаря аннотации @Repository этот компонент становится доступным в нашем приложении
 */
@Repository
public interface VisitRepository extends CrudRepository<Visit, Long> {
}
