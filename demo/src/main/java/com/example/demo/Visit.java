package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Сбор статистики посещений — каждый раз,
 * когда кто-то заходит на главную страницу, пишем это в базу.
 */

@Entity
public class Visit {
    @Id
    @GeneratedValue
    public Long id;

    public String description;
}
